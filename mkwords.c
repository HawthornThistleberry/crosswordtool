#include <stdio.h>
#include <math.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int len, skip;
  char s[50], last[50], *cp, fname[12];
  FILE *fp = NULL;

  for (len=0;len<25;len++) {
    sprintf(fname,"words%d.txt",len);
    remove(fname);
    }
  last[0]='\0';

  while (gets(s)) {

    /* remove apostrophes, convert to lowercase */
    cp = s;
    while (*cp) {
      if (*cp=='\'') {
          strcpy(cp,cp+1);
        } else {
          *cp = tolower(*cp);
          cp++;
        }
      }

    /* if it's too long, skip it */
    len = strlen(s);
    if (len>=25) continue;

    /* if it has non-alphabetic characters, skip it */
    skip = 0;
    for (cp=s; *cp; cp++) {
      if (*cp<'a' || *cp>'z') skip = 1;
      }
    if (skip) continue;

    /* if it's the same as last line, skip it */
    if (!stricmp(s,last)) continue;

    /* write it to the right file (opening it if needed) */
    sprintf(fname,"words%d.txt",len);
    if ((fp=fopen(fname,"a"))==NULL) {
         fprintf(stderr,"Error: can't open %s\n",fname);
         return 1;
      }
    fprintf(fp,"%s\n",s);
    fclose(fp);
    printf("%s                         \r",s);
    strcpy(last,s);
    }

  return 0;

  }
