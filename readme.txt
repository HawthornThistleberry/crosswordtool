                          Crossword Tool  v1.0

(c) 1997 by Frank J. Perricone (hawthorn@foobox.com).  All rights reserved.

WHAT IS CROSSWORD TOOL?

I wrote Crossword Tool to help me *make* crossword puzzles, but you can
also use it to help you *solve* them.  It's a very simple program.  All
you do is generate a "pattern" based on what letters you know and what
letters you don't.  Letters that you don't are replaced with a special
punctuation symbol.  The symbols are:

  ?  match any letter
  @  match any vowel (aeiouy)
  &  match any consonant (bcdfghjklmnpqrstvwxyz)

Yes, y is on both lists, just in case.  So for instance, if you have a
five-letter word beginning with a T, ending with an L, and where you
know (because of a cross-clue) that the second letter is a vowel, you
might enter t@??l.  Then click on "Generate Word List" or press Enter,
and Crossword Tool will go through its word list and create a list of
everything that matches.

You can click the "Stop Generating" button to stop the generation before
it's done.  You can resize the window to see more words at a time if you
like.  It'll even remember the size you made it last time when you run
it the next time.  That's all there is to it.

INSTALLING IT

This program comes in two archives; normal size, and Microsoft size.
The former is for use by anyone who's ever installed anything that was
written using Visual Basic 4.0, and so who already has all those shared
libraries.  That is, almost everyone.  Just unarchive it into a fresh
new folder (I use C:\Program Files\Crossword Tool) and then make a
shortcut to the EXE file.

If this doesn't work and you get ugly errors about missing files, you'll
need the Microsoft size package, which includes every bloated shared
library file that Microsoft could find lying about in time to stuff it
into the VB4 runtime.  Plus it includes a handy-dandy install program,
which doesn't really do anything.  As this readme file is in both
archives, maybe you already have that.  There should be no harm in using
the Microsoft size even if you don't need it, but don't quote me on
that.

LIMITATIONS

Note that Crossword Tool is far from infallible.  Here are its known
limitations:

1. The word list is missing lots of things like names, mythological
words, acronyms, etc. that you can use in a crossword.  It's fairly
easy to edit the word list; it's just a bunch of files named words2.txt
to words24.txt, plain ASCII files with the words of that length in
alphabetical order.  (The current version of the program takes no
advantage of the order, however, nor does it require words be unique;
they will show up in the results just as they appear in the file,
though, out of order and duplicated if they're out of order and
duplicated in the words*.txt files.)

It's sometimes easier to make words files by taking one huge file of
words and sifting it into the 23 files.  I've written a C program
mkwords.c which is included in this archive, which does this.
Redirect a word list into it and it will strip out any apostrophes
and any word that has any other form of punctuation, attempt to
strip duplicates (if they happen to be next to one another in the
source file), and create the 23 words*.txt files from it.  You
could join the files that are provided together, append another file
of your favorite words, sort the result, then run mkwords.exe on
it to create a fresh new bunch of words*.txt files.

2. Crossword Tool doesn't take advantage of the fact that words are
sorted within each file, so it does an exhaustive search of the entire
file.  And it's running at Visual Basic 4.0 speeds.  Even so, thirty
seconds to go through every seven-letter word in the dictionary, on a
Pentium/60, seems like a bargain.  Someone else want to make it use a
little smarts to index into the file based on the first letter (if it's
not a ?) and read only those words that might match?  I don't think it's
worth it.

3. Crossword Tool will never find a solution that is comprised of two or
more words.  And of course it won't help you much with trick answers
like ones that include punctuation or digits.

4. Wouldn't it be nice if you could specify that the second letter is,
say "either a vowel, an H, a T, or an N"?  If you've done crosswords you
konw what I mean.  The majority of the time, for instance, the letter
after a leading T can be a vowel, H, R, or W, but rarely anything else.
Lots of other letter combinations and sound combinations are rare or
impossible allowing you to narrow down the possibilities.  I don't see
any way to implement something like this without a complete redesign,
though, where each letter in the pattern lets you pop up something that
lets you check off what letters it can be.  (So if you know the letter,
that means just one letter is checked; if you know it's a vowel, then
six letters are checked; etc.)  Anyone else want to do this?  If not,
maybe for v2.0.

WHAT DOES IT COST?

Crossword Tool is POSTCARDWARE.  You can use it all you like and share
it with anyone you want.  There are only three limits:

* If you do anything to the source code to improve or modify it, please
let me know.
* If you distribute this, leave credit for me in it.  After all, I *did*
write it.
* If you like it, please send me a postcard showing where you live.
Mail it to:

   Frank J. Perricone
   20 Trickle Brook Drive
   Orange VT 05641

Don't forget to tell me what program you're "registering" and include
your e-mail address.  (Please don't send me one of those cutesie
Internet Postcards thingies, unless it actually shows a pic of the place
you live.)

In response I'll be sure to let you know if I upgrade that program or
get a better word list.  If you don't want to send me a postcard, that's
fine too; no guilt trips here.  I'd just like to hear who's using this
and see where they are.

The source code is included for Visual Basic v4.0.  Have fun customizing
it, but please let me know at hawthorn@foobox.com what you come up with.
I'd also love to see your improved word list so I can share it with
other users of the program.

